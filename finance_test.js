var request = require('request-json');
var moment = require('moment');
var config = require('./config');
var util = require('./util');
var readline = require('readline-sync');
var fs = require('fs');

var client = request.createClient(config.url);
client.setBasicAuth(config.devKey,'');

//client.get('/currencies', util.log);

// Show accounts:
//client.get("/accounts", util.log);

// Show general stuff about me
//client.get('/me', util.log);

// Get entries in some date range
var data = {
    from:"2015-09-01",
    to:"2017-09-30"
};

var url = '/entries?from='+data.from+'&to='+data.to;
//console.log(url);
//client.get(url, util.log);

// Delete all entries
output = [];
var enable = false;
if(enable)
client.get(url, function(err, res, body){
    if( Array.isArray( body ) ) {
        body.forEach(function(value){
            output.push(value);
        });
    } else {
        output.push(body);
    }
    output = output.filter(val => val.desc === "automated");
    console.log(output);
    var index = 0;

    if(output.length == 0){
        process.exit();
    }

    if(!readline.keyInYN("Really delete " + output.length + " entries?")){
        process.exit();
    }
    var backup = JSON.stringify(output);
    var date = Date.now();
    fs.writeFileSync("entries_backup"+date+".json", backup);

    var just_get = true;
    output.forEach(function(value){
        if(!just_get)
        client.del('/entries/'+value.id,function(err, res, body){
            if (err) {
                return console.log(err);
            }
            console.log("deleting id"+value.id);
            if( ++index == output.length){
                process.exit()
            }
        });
    });
});

client.get('/categories', function(err, res, body){
    output=[];
    body.forEach(function(value){
        output.push({id:value.id, name:value.name});
    });
    console.log(output);
});

// client.get('/tags', function(err, res, body){
//     output=[];
//     body.forEach(function(value){
//         output.push(value.name);
//     });
//     console.log(output);
// });