var output = module.exports = {};

// Helper function to log out the response.
output.log = function(err, res, body){
    if( err ) {
        console.log('error:', err); // Print the error if one occurred
    }    
    console.log('statusCode:', res && res.statusCode); // Print the response status code if a response was received
    output = [];
    if( Array.isArray( body ) ) {
        body.forEach(function(value){
            output.push(value);
        });
    } else {
        output.push(body);
    }
    console.log(output);
}