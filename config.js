// configuration file for server-specific stuff
var config = module.exports
= {
    // The real deal
    devKey:'1054d409-c2cf-4820-acdd-6d9b99ccebf94f39a92e-3ac7-4d20-8bc3-387727f58bf6',
    categoryDB:"categories.json",
    tagDB:"tags.json",
    database:"database.txt",
    url:'https://api.toshl.com/',
    accountId:'2687667', 
    fileName:'santander_all_values.csv' 
};
//= {
//     // Debug
//     devKey:'b4786549-c698-4ac2-90a8-77bf31ae2bfa513deb24-f53f-4499-9f7b-08fbf59a8c2f',
//     categoryDB:"categories.debug.json",
//     database:"database.debug.txt",
//     url:'https://api.toshl.com/',
//     accountId:'2911554', 
//     fileName:'julytosept.csv' 
// };