var request = require('request-json');
var moment = require('moment');
var config = require('./config');
var util = require('./util');
var fs = require('fs');
var readline = require('readline-sync');

//Setup client and authentication
var client = request.createClient(config.url);
client.setBasicAuth(config.devKey,'');

//client.post('/entries', new_entry, util.log );

categories={};
var categories_exist = fs.existsSync(config.categoryDB);
if( !categories_exist ){
    client.get('/categories', function(err, res, body){
        body.forEach(function(value){
            categories[value.id]={name:value.name,
                type:value.type};
        });
        var to_disk = JSON.stringify(categories);
        fs.writeFileSync(config.categoryDB, to_disk );
        process.exit();
    });
} else {
    var file = fs.readFileSync(config.categoryDB);
    categories = JSON.parse(file);
}

// tags={};
 var tags_exist = true;//fs.existsSync(config.tagsDB);
// if( !tags_exist ){
//     client.get('/tags', function(err, res, body){
//         body.forEach(function(value){
//             tags[value.id]={name:value.name,
//                 type:value.type};
//         });
//         var to_disk = JSON.stringify(tags);
//         fs.writeFileSync(config.tagsDB, to_disk );
//         process.exit();
//     });
// } else {
//     var file = fs.readFileSync(config.tagsDB);
//     categories = JSON.parse(file);
// }

//YUHU, it works

// Next steps
// 1. open the santander download
// 2. iterate through the strings, 1st column, date, betrag, hmm, 
// 3. look for specific strings to move it into the correct column.
// it should not do anything if it doesn't know

var fs = require('fs');
var csvparse = require('csv-parse');

var database_filename = config.database;
var database = {
    negative:{},
    positive:{}
};
var ignore = {};
if(categories_exist && fs.existsSync(database_filename)){
    database = JSON.parse(fs.readFileSync(database_filename));
    console.log("Using database with positive entries:");
    for( var key in database.positive ){
        var cat = categories[database.positive[key]];
        console.log(key + "  -->>  " + cat.name + " type " + cat.type);
    }
    console.log("Using database with negative entries:");
    for( var key in database.negative ){
        var cat = categories[database.negative[key]];
        console.log(key + "  -->>  " + cat.name + " type " + cat.type);
    }    
}

var rejects=[];

function ask_user_to_add_category(strings, positive){
    var cat_list = [];
    var key_list = [];
    for(var key in categories){
        var obj = categories[key];
        if(positive && obj.type == "income" || !positive && obj.type == "expense") {
            key_list.push(key);
            cat_list.push(categories[key].name);
        }
    }
    
    var index_values = readline.keyInSelect(strings,"Which keyword");
    if( index_values == -1 ){
        var which_ignore = readline.keyInSelect(strings, "Which ignore?");
        ignore[strings[which_ignore]] = true;
        return {valid:false};
    }
    var index = readline.keyInSelect(cat_list, "Which category?");
    if( index == -1 ){
        return {valid:false};
    }
    var str_used = strings[index_values];
    // if it doesn't work, then story the keys of category (like cat_list)
    var cat_index = key_list[index];
    if(positive)
        database.positive[str_used] = cat_index;
    else
        database.negative[str_used] = cat_index;
    //console.log(database);
    return {valid:true,categoryId:key_list[index],
        str_used:str_used};
}

to_post = [];

function upload(){
    to_post.forEach(function(val){
        console.log("Date: " +
        val.date + " for " + val.amount +
         " with category " + categories[val.category].name +
         " and type " + categories[val.category].type );
         console.log("\t because of " + val.extra.string_used );
    });
    if(to_post.length == 0){
        console.log("Nothing to upload, done!");
        process.exit();
    }
    var post = readline.keyInYN("Should I continue?");
    if(post){
        var index = 0;
        var data_count = to_post.length;
        to_post.forEach(function(val){
            client.post('/entries', val, function(err,res,body){
                if(err){
                    console.log(err);
                }
                else if(res.statusCode != 201) {
                    console.log("Something strange should be investigated.");
                    console.log(body);
                }
                if( ++index == data_count ){
                    console.log("Finished uploading " + data_count + " items.");
                    process.exit();
                }
            } );
        });
    } else {
        console.log("please fix the dictionary");
        //var remove_from_dic = readline.keyInYN("Should I remove from dictionary?");
        // if( remove_from_dic ){
        //     to_post.forEach(function(val){
        //         delete database[val.extra.string_used];
        //     });
        //     fs.writeFileSync(database_filename, JSON.stringify(database));
        // }
        process.exit();
    }
}

existing_payments = {};
function process_parse(err, payments) {
    var index_database = 0;
    var failed = 0;
    var data_count = payments.length;

    // Iterate each payment
    payments.forEach(function(value){
        strings=[];

        //Cleanup the values
        value.forEach(function(str){
            //console.log(str);
            var split = str.split(" ");
            var clean_split = split.filter( item => item.length > 2);
            clean_split.forEach(function(str) {
                strings.push(str.toLowerCase());
            });
        });

        while(strings.length > 35)
            strings.pop();

        // Find the category, if none found, add it to the list of rejects.
        categoryId = 0;
        var str_used = "";

        var amount = value[3].split(" ")[0].replace(",",".");
        var positive = amount.indexOf('-') == -1;
        var skip = false;
        strings.forEach(function(str){
            var db = positive ? database.positive : database.negative;
            var obj = db[ str.toLowerCase() ];
            if( obj != undefined ) {
                str_used = str;
                categoryId = obj;
            }
            if( ignore[str] == true ){
                readline.question("FYI, ignoring " + str + " of amount:" + amount);
                skip = true;
                return;
            }
        });

        var valid_category = categoryId != 0;
        if(!valid_category && !skip) {
            var res = ask_user_to_add_category(strings,positive);
            //get the category and send it to toshl!!!
            if(!res.valid){
                index_database++;
                failed++;
                return;
            } else {
                categoryId = res.categoryId;
                str_used = res.str_used;
            }
        }

        // Don't try to add one we wanted to skip
        if(skip){
            return;
        }
        var date = moment(value[0], "DD.MM.YYYY");
        var new_entry = {
            amount:amount,
            currency:{ code:"EUR"},
            date:date.format("YYYY-MM-DD"),
            account:config.accountId,
            category:categoryId,
            desc:"automated",
            extra:{string_used: str_used}
        };
        var existing_payment = existing_payments[new_entry.date];
        var contains_payment = existing_payment && existing_payment.findIndex( value => value == amount)
        if(contains_payment == undefined || contains_payment == -1){
            to_post.push(new_entry);
            console.log("Adding entry on: " + new_entry.date + " for " + new_entry.amount);
        }
        else {
            console.log("Entry already exists on: " + new_entry.date + " for " + new_entry.amount);
        }
            
        fs.writeFileSync(database_filename, JSON.stringify(database));
        if( ++index_database == data_count ){
            console.log("DONE!!");            
        }
    });
    upload();
}

function parse_csv(){
    var parser = csvparse({delimiter: ';'}, process_parse);
    if(categories_exist){
        fs.createReadStream(__dirname+'/'+config.fileName).pipe(parser);
    }
}

var parse = require('parse-link-header');

function get_current_entries() {
    var data = {
        from:"2015-07-01",
        to:"2017-09-30"
    };    
    var url = '/entries?from='+data.from+'&to='+data.to+'&per_page=500&page=';
    
    function process_entries(err, res, body){
        var parsed = parse(res.headers.link);
        if( Array.isArray( body ) ) {
            body.forEach(function(value){
                if(existing_payments[value.date] == undefined)
                    existing_payments[value.date] = [];
                existing_payments[value.date].push(value.amount);
            });
        } else {
            existing_payments.push(body);
        }
        if(parsed.next != undefined){
            client.get(url + parsed.next.page, process_entries);
        } else {
            // No more entries to check, so now iterate through the csv.
            console.log(existing_payments);
            parse_csv( );
        }
    }
    client.get(url + '0', process_entries);
}

if(categories_exist && tags_exist)
    get_current_entries();